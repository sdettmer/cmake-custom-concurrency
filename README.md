# Example Cmake Custom Commands

CMake provides the `add_custom_command()` and `add_custom_target()` functions
to control running helper processess such as source generators or lint tools

This little repo aims to show the difference between the two when used in
modern multi-core builds.

## Naive Example

Uses `add_custom_command()` is error prone on multi-core builds.  The
documentation of `add_custom_command()` now warns of this in the small-print
but before cmake 3.1 it did not.

Many google hits for cmake custom commands find the earlier 3.0.3 cmake docs.

## More Robust Example

Uses `add_custom_target()` instead of `add_custom_command()`. The execution
order is clearly defined but there is no implicit dependency setup between the
generator and the consumers.

This is fully robust but has the disadvangage that the custom target is
executed every time so incremental builds are less useful

## Mixed Targets Example

This is the reccomended way of doing custom generation of source using an
external tool according to the current (3.10) cmake docs.

We have a custom target used to control the custom command execution.

The downside of this is that it is easy to make a mistake in your consumers

## Once Based Example

This is sort of a safety net, it prevents two executions of the same custom
command from overlapping. 
