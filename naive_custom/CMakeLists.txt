cmake_minimum_required(VERSION 3.7)

Project(naive_custom C)

#
# This can go wrong with concurrent builds (make -jx) because
# each library will cause the custom command to be run.
#
# With moste generators (make, ninja) doing make -j1 will mean
# the custom command only gets executed once but you can't
# always depend on this.
#
# Some generaotrs might work without issue, but that is by luck really
#
add_custom_command(
  OUTPUT test1.c test1.h test2.c test2.h test3.c test3.h
  COMMAND python ${CMAKE_CURRENT_LIST_DIR}/../generator.py
  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
  )


add_library(test1
  STATIC
  test1.c)

add_library(test2
  STATIC
  test2.c)

add_library(test3
  STATIC
  test3.c)
